import React from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as actions from '../actions';

const Counter = ({counter,inc,dec,rnd}) => {
    return (
        <div id="root" className="jumbotron">
            <h2>{counter}</h2>
            <button 
                className="btm btn-primary btn-lg"
                onClick={inc}>+</button>
            <button 
                className="btm btn-primary btn-lg"
                onClick={dec}>-</button>
            <button 
                className="btm btn-primary btn-lg"
                onClick={rnd}>rnd</button>
        </div>
    )
}

const mapStateToProps = (state) =>{
    return {
        counter: state
    }
}
const mapDisoatchToProps = (dispatch) =>{
    return bindActionCreators(actions,dispatch);    
}

export default connect(mapStateToProps,mapDisoatchToProps)(Counter);